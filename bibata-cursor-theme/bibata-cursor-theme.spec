%global         source_name Bibata_Cursor

Name:		bibata-cursor-theme
Version:	0.4.1
Release:	2%{?dist}
Summary:	Material Based Cursor Theme

License:	GPLv3+
URL:		https://github.com/KaizIqbal/Bibata_Cursor
Source0:	%{url}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	inkscape
BuildRequires:	gnome-themes-extra
BuildRequires:	xcursorgen


%description
Material Based Cursor Theme.
	
%prep
%autosetup -n %{source_name}-%{version}

%build
./build.sh

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p "$RPM_BUILD_ROOT/%{_datadir}/icons"
mv $RPM_BUILD_DIR/%{source_name}-%{version}/Bibata_Ice "$RPM_BUILD_ROOT/%{_datadir}/icons"
mv $RPM_BUILD_DIR/%{source_name}-%{version}/Bibata_Oil "$RPM_BUILD_ROOT/%{_datadir}/icons"
mv $RPM_BUILD_DIR/%{source_name}-%{version}/Bibata_Amber "$RPM_BUILD_ROOT/%{_datadir}/icons"
	
%files
%license LICENSE
%doc README.md
%{_datadir}/icons/*
	
%changelog

* Sun Oct 13 2019 Muhammad Ahmad <muhalantabli@gmail.com>
- New Release - v0.4.1

