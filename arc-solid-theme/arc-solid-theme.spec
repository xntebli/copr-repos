%global common_configure --disable-transparency --with-gnome-shell=3.34
%global         source_name arc-theme

%global common_desc Arc Solid is a flat theme without transparent elements for GTK 3, GTK 2 and GNOME Shell, Unity, Pantheon, Xfce, MATE, Cinnamon, Budgie Desktop.

Name:		arc-solid-theme
Version:	20190917
Release:	3%{?dist}
Summary:	A flat theme without transparent elements

License:	GPLv3+
URL:		https://github.com/arc-design/arc-theme
Source0:	%{url}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gtk3-devel
BuildRequires:	gtk-murrine-engine
BuildRequires:	inkscape
BuildRequires:	optipng
BuildRequires:	sassc

Requires:	filesystem
Requires:	gnome-themes-extra
Requires:	gtk-murrine-engine

%description
%{common_desc}
	
%prep
%autosetup -n %{source_name}-%{version}
%{_bindir}/autoreconf -fiv

%build	
%configure %{common_configure}
%make_build

%install	
%make_install
	
%files
%license AUTHORS COPYING
%doc README.md
%{_datadir}/themes/*
	
%changelog

* Sat Oct 19 2019 Muhammad Ahmad <muhalantabli@gmail.com>
- All Default DE Support - 20190917-3


